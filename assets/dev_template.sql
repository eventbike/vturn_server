BEGIN TRANSACTION;
INSERT INTO `client` (id,name,key,mode) VALUES (1,'static_normal',"m6hOEZsdwTjmSHaQ",0),
 (2,'static_admin','nMRkxVemTjxFHeCa',4);
INSERT INTO `team` (id,name,shortname,songfile) VALUES (1,'eventbike','EVB','Netzwerk.mp3'),
 (2,'Lehrerteam','LET','ACDC_irgendwas.mp3'),
 (3,'AffengeileAffen','AFF',''),
 (4,'Katholische Kirche','KAK','');
INSERT INTO `slot` (id,team_id,group_id) VALUES (1,1,1),
 (2,2,1),
 (3,3,2),
 (4,4,2);
INSERT INTO `match` (id,slot_home,slot_visiting,score_home,score_visiting,duration,schedule) VALUES (1,1,2,2,1,0,NULL),
 (2,3,4,0,0,360,NULL);
INSERT INTO `group` (id,name) VALUES (1,'A'),
 (2,'B'),
 (3,'C'),
 (4,'D');
COMMIT;
