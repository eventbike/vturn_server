BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `team` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT,
	`shortname`	TEXT,
	`songfile`	TEXT
);
CREATE TABLE IF NOT EXISTS `slot` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`team_id`	INTEGER,
	`group_id`	INTEGER,
	FOREIGN KEY (team_id) REFERENCES `team` (id),
	FOREIGN KEY (group_id) REFERENCES `group` (id)
);
CREATE TABLE IF NOT EXISTS `setting` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`key`	TEXT,
	`value`	TEXT
);
CREATE TABLE IF NOT EXISTS `match` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`slot_home`	INTEGER,
	`slot_visiting`	INTEGER,
	`score_home`	INTEGER,
	`score_visiting`	INTEGER,
	`duration`	INTEGER,
	`schedule`	INTEGER,
	FOREIGN KEY (slot_home) REFERENCES `slot` (id),
	FOREIGN KEY (slot_visiting) REFERENCES `slot` (id)
);
CREATE TABLE IF NOT EXISTS `group` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT
);
CREATE TABLE IF NOT EXISTS `client` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`key`	TEXT,
	`name`	TEXT,
	`mode`	INTEGER,
	`last_active`	INTEGER
);
COMMIT;
