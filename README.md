# vturn_server

> **Sadly, we didn't find the time and / or motivation to complete this project. It was scheduled for an event in December 2021, but we realized it without this WIP piece of software and don't currently plan on continuing its development.**

VTurn is a sports tournament management app. 

The eventbike_zero is a students collective doing event technology in Potsdam, Germany. One of their recurring events include soccer tourneys, their management shall be made simple with this all-inclusive apps.

This is the backend code and acts as a server. It opens a sqlite database as project file and serves the frontend as well as API calls to clients, which should not need any software setup, but simply open the frontend in a webbrowser. The idea is to use this software as flexible as possible and allow easy scaling by using more machines without complicated software setup.

## Roadmap

The current-state might be considered as better-than-nothing-Alpha. The development is scheduled to happen throughout the year 2021.

In the first step, this software should allow the creation of teams and assign them into matches, allow tracking of points and allow automatic calculation of matches based on fixed rules.
*Help needed:* We don't yet have an idea on how to allow defining flexible rules for the game logic. It would be nice to support that, so different sports can be supported.

Some specifically required features also include some interface for a livestream overlay as well as a goal sound per team which is automatically played once a team scores.

