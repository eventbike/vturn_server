module main

import os
import sqlite
import app
import api

fn main() {
	if os.args.len > 1 {
		match os.args[1] {
			'start' {
				if os.args.len > 2 {
					open_project(os.args[2])
				}
			}
			'new' {
				if os.args.len > 2 {
					app.new_project(os.args[2])
				} else {
					println('Please provide a name for your project!')
				}
			}
			else {
				println('Either use `start` or `new` parameter, followed by a filename.')
			}
		}
	}
}

fn open_project(file string) {
	filename := if os.exists(file + '.db') { file + '.db' } else { file }
	if os.exists(filename) {
		mut db := sqlite.connect(filename) or { panic(err) }
		api.run(db)
		db.close() or { panic(err) }
	} else {
		println("Could not find project '$filename'.")
	}
}
