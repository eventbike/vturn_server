module app

import sqlite
import os

pub fn new_project(filename string) {
	if os.exists(filename) {
		match os.input('Project already created! Do you want to override it? Y(es) / N(o)') {
			'Y' {
				println('Project will be overwritten!')
			}
			'N' {
				println("Ok, we'll keep it for you :) Try it again with another name!")
				exit(1)
			}
			else {
				println('Is this really that hard? Try it again with Y or N!')
				exit(1)
			}
		}
	}
	os.create(filename) or { panic(err) }
	mut db := sqlite.connect(filename) or { panic(err) }
	if db_import(db, 'assets/structure.sql') {
		println('Project and basic db structure were created')
	} else {
		println('Project creation failed')
	}
	$if dev_template ? {
		if db_import(db, 'assets/dev_template.sql') {
			println('Additional development data imported')
		}
	}
	db.close() or { panic(err) }
}

pub fn db_import(db sqlite.DB, filename string) bool {
	if os.exists(filename) {
		file := os.read_file(filename) or { panic(err) }
		statements_array := file.split(';')
		for statement in statements_array {
			if statement != '\n' {
				if db.exec_none(statement) != 101 {
					println('Query failed: $statement')
					return false
				}
			}
		}
		return true
	} else {
		return false
	}
}
