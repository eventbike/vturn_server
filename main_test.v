module main

import sqlite
import os
import app

const testdb = '.testsuite.db'

fn testsuite_begin() {
}

fn test_db_import() {
	os.create(testdb) or { panic(err) }
	mut db := sqlite.connect(testdb) or { panic(err) }
	assert app.db_import(db, 'assets/structure.sql')
	os.rm(testdb) or { panic(err) }
}

fn test_new_project() {
	app.new_project(testdb)
	assert os.exists(testdb)
	os.rm(testdb) or { panic(err) }
}

fn testsuite_end() {
	if os.exists(testdb) {
		os.rm(testdb) or { panic(err) }
	}
}
