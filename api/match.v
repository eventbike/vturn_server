module api

import vweb
import json
import sqlite

struct MatchResponse {
	id             int
	team_home      Team
	team_visiting  Team
	score_home     int
	score_visiting int
	duration       int
	schedule       int
}

// `match/get` returns all data for ONE game (including the same information as "get_all_data()" ?)
['/api/match/get'; post]
pub fn (mut app Vturn) match_get() vweb.Result {
	if true {
		sent_match := json.decode(Match, app.form['data']) or {
			return app.json(json.encode(ApiError{
				message: 'Please specify an id!'
			}))
		}
		return app.json(json.encode(sent_match.get_match(app.db)))
	} else {
		return app.json(json.encode(ApiError{
			message: 'Insufficient permissions'
		}))
	}
}

// `/api/match/create` creates a Match and returns the full one
// client mode: 3
// Match => Match
['/api/match/create']
pub fn (mut app Vturn) match_create() vweb.Result {
	if check_client(app).mode >= 3 {
	}
	return app.json('')
}

// returns a Match
fn (query Match) get_match(db sqlite.DB) MatchResponse {
	// query needed information from DB
	// ToDo: Check if V ORM supports getting all SQLite constraints from struct
	match_ := sql db {
		select from Match where id == query.id
	}
	slot1 := sql db {
		select from Slot where id == match_.slot_home
	}
	slot2 := sql db {
		select from Slot where id == match_.slot_visiting
	}
	println(slot2)
	team1 := sql db {
		select from Team where id == slot1.team_id
	}
	team2 := sql db {
		select from Team where id == slot2.team_id
	}
	// assemble response
	return MatchResponse{
		id: match_.id
		team_home: team1
		team_visiting: team2
		score_home: match_.score_home
		score_visiting: match_.score_visiting
		duration: match_.duration
		schedule: match_.schedule
	}
}
