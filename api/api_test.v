module api

import os
import sqlite
import net.http
import json
import time
import app

const testdb = '.api_test.db'

const testport = port + 1

fn testsuite_begin() {
	if os.exists(api.testdb) {
		os.rm(api.testdb) or { panic(err) }
	}
	app.new_project(api.testdb)
	db := sqlite.connect(api.testdb) or { panic(err) }
	assert app.db_import(db, 'assets/dev_template.sql')
	// run webserver in a new thread and wait 1sec for it to start
	go run(db)
	time.sleep(1)
}

fn test_connectivity() {
	response := http.get('http://localhost:$port/') or { http.Response{} }
	assert response.text == 'Welcome to vturn. Find more on https://codeberg.org/eventbike/vturn_server :-)'
}

// ToDo: implement a check to see if the CORS stuff is set correctly
// (set_cors() in api.v)
fn test_cors() {
}

fn test_client_register() {
	response := http.post('http://localhost:$port/api/client/register', 'data={"name":"api_test"}') or {
		http.Response{}
	}
	println(response.text)
	client := json.decode(Client, response.text) or { Client{} }
	assert client.name == 'api_test'
	assert client.id == 3
	assert client.key.len == 16
	println('"$client.key"')
	assert client.mode == 0
}

fn test_client_register_2() {
	request := Client{
		name: 'api_test2'
		id: 33
		key: 'abcd'
		mode: 5
	}
	response := http.post('http://localhost:$port/api/client/register', 'data=${json.encode(request)}') or {
		http.Response{}
	}
	client := json.decode(Client, response.text) or { Client{} }
	assert client.name == 'api_test2'
	assert client.id == 4
	assert client.key.len == 16
	assert client.mode == 0
}

fn test_client_list_nodata() {
	response := http.post('http://localhost:$port/api/client/list', '') or { http.Response{} }
	data := json.decode(ApiError, response.text) or {
		ApiError{
			success: true
		}
	}
	assert data.message == 'Insufficient permissions'
	assert data.success == false
}

fn test_client_list_nopermission() {
	request := Client{
		name: 'static_normal'
		id: 1
		key: 'm6hOEZsdwTjmSHaQ'
		mode: 4
	}
	response := http.post('http://localhost:$port/api/client/list', 'client=${json.encode(request)}') or {
		http.Response{}
	}
	data := json.decode(ApiError, response.text) or {
		ApiError{
			success: true
		}
	}
	assert data.message == 'Insufficient permissions'
	assert data.success == false
}

fn test_client_list_valid() {
	request := Client{
		id: 2
		key: 'nMRkxVemTjxFHeCa'
	}
	response := http.post('http://localhost:$port/api/client/list', 'client=${json.encode(request)}') or {
		http.Response{}
	}
	data := json.decode([]Client, response.text) or { []Client{} }
	assert data.len == 4
	assert data[0].id == 1
	assert data[0].mode == 0
	assert data[1].name == 'static_admin'
	assert data[1].key == 'nMRkxVemTjxFHeCa'
	assert data[1].mode == 4
	assert data[2].mode == 0
	assert data[3].id == 4
	assert data[3].name == 'api_test2'
}

fn test_client_update_norequest() {
	response := http.post('http://localhost:$port/api/client/update', '') or { http.Response{} }
	data := json.decode(ApiError, response.text) or {
		ApiError{
			success: true
		}
	}
	assert data.message == 'Insufficient permissions'
	assert data.success == false
}

fn test_client_update_nodata() {
	auth := Client{
		id: 2
		key: 'nMRkxVemTjxFHeCa'
	}
	response := http.post('http://localhost:$port/api/client/update', 'client=${json.encode(auth)}') or {
		http.Response{}
	}
	data := json.decode(ApiError, response.text) or {
		ApiError{
			success: true
		}
	}
	assert data.message == 'No data sent'
	assert data.success == false
}

fn test_client_update_nopermission() {
	auth := Client{
		name: 'static_normal'
		id: 1
		key: 'm6hOEZsdwTjmSHaQ'
		mode: 4
	}
	request := Client{
		id: 1
		key: 'm6hOEZsdwTjmSHaQ'
		name: 'Hello there'
		mode: 2
	}
	response := http.post('http://localhost:$port/api/client/update', 'client=${json.encode(auth)}&data=${json.encode(request)}') or {
		http.Response{}
	}
	data := json.decode(ApiError, response.text) or {
		ApiError{
			success: true
		}
	}
	assert data.message == 'Insufficient permissions'
	assert data.success == false
}

fn test_client_update() {
	auth := Client{
		id: 2
		key: 'nMRkxVemTjxFHeCa'
	}
	request := Client{
		id: 1
		key: 'm6hOEZsdwTjmSHaQ'
		name: 'Hello there'
		mode: 2
	}
	response := http.post('http://localhost:$port/api/client/update', 'client=${json.encode(auth)}&data=${json.encode(request)}') or {
		http.Response{}
	}
	data := json.decode(Client, response.text) or {
		Client{
			id: 2
		}
	}
	assert data.id == 1
	assert data.key == 'm6hOEZsdwTjmSHaQ'
	assert data.name == 'Hello there'
}

fn test_match_get_direct() {
	mut db := sqlite.connect(':memory:') or { panic(err) }
	assert app.db_import(db, 'assets/structure.sql')
	assert app.db_import(db, 'assets/dev_template.sql')
	assert Match{
		id: 1
	}.get_match(db) == MatchResponse{
		id: 1
		team_home: Team{
			id: 1
			name: 'eventbike'
			shortname: 'EVB'
			songfile: 'Netzwerk.mp3'
		}
		team_visiting: Team{
			id: 2
			name: 'Lehrerteam'
			shortname: 'LET'
			songfile: 'ACDC_irgendwas.mp3'
		}
		score_home: 2
		score_visiting: 1
		duration: 0
		schedule: 0
	}
}

fn test_match_get_api() {
	auth := Client{
		id: 2
		key: 'nMRkxVemTjxFHeCa'
	}
	request := Match{
		id: 2
	}
	response := http.post('http://localhost:$port/api/match/get', 'client=${json.encode(auth)}&data=${json.encode(request)}') or {
		http.Response{}
	}
	data := json.decode(MatchResponse, response.text) or { MatchResponse{} }
	assert data == MatchResponse{
		id: 2
		team_home: Team{
			id: 3
			name: 'AffengeileAffen'
			shortname: 'AFF'
			songfile: ''
		}
		team_visiting: Team{
			id: 4
			name: 'Katholische Kirche'
			shortname: 'KAK'
			songfile: ''
		}
		score_home: 0
		score_visiting: 0
		duration: 360
		schedule: 0
	}
}

fn testsuite_end() {
	if os.exists(api.testdb) {
		os.rm(api.testdb) or { panic(err) }
	}
}
