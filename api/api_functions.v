module api

import json

fn check_client(app Vturn) Client {
	mut client := json.decode(Client, app.form['client']) or {
		Client{
			name: 'Some client who does not know much about valid JSON.'
		}
	}
	client.key = client.key.replace("'", '"')
	client.mode = app.db.q_int("SELECT mode FROM client WHERE id = $client.id and key = '$client.key'")
	// ToDo: figure out why /nothing/ like that works
	/*
	return sql db {
		select from Clients where id == c.id && key == c.key.replace("'","'")
	}[0].mode
	*/
	return client
}
