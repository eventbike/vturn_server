module api

import vweb

struct ApiError {
	success bool
mut:
	message string
}

struct Setting {
	id    int
	key   string
	value string
}

struct Slot {
	id       int
	team_id  int
	group_id int
}

struct Team {
	id        int
	name      string
	shortname string
	songfile  string
}

struct Match {
	id             int
	slot_home      int
	slot_visiting  int
	score_home     int
	score_visiting int
	duration       int
	schedule       int
}

pub fn (mut app Vturn) index() vweb.Result {
	return app.text('Welcome to vturn. Find more on https://codeberg.org/eventbike/vturn_server :-)')
}

// set_cors responds to all OPTIONS HTTP requests in /api/*/* to allow development. 
// when compiled using -d dev_local, it also adds the Access-Control-Allow-{Methods, Headers, Origin} headers to *
['/api/:realm/:request'; options]
pub fn (mut app Vturn) set_cors(api string, realm string) vweb.Result {
	$if dev_local ? {
		app.add_header('Access-Control-Allow-Methods', '*')
		app.add_header('Access-Control-Allow-Headers', '*')
		app.add_header('Access-Control-Allow-Origin', '*')
	}
	return app.text('')
}

// get_goal_sound returns an URL of the goal sound file for one team.
pub fn (mut app Vturn) get_goal_sound() vweb.Result {
	return app.json('')
}

// edit_game edits a game.
pub fn (mut app Vturn) edit_game() vweb.Result {
	return app.json('')
}

// update_game_status updates a game status including the game id, period and goals.
pub fn (mut app Vturn) update_game_status() vweb.Result {
	return app.json('')
}

// create_team creates a team and returns it's id.
pub fn (mut app Vturn) create_team() vweb.Result {
	return app.json('')
}

// edit_team edits a team.
pub fn (mut app Vturn) edit_team() vweb.Result {
	return app.json('')
}
