module api

import vweb
import json
import time
import rand

struct Client {
mut:
	id          int
	key         string
	name        string
	mode        int
	last_active int
}

// client_register registers a new client and sends back all client data
['/api/client/register'; post]
pub fn (mut app Vturn) client_register() vweb.Result {
	sent_client := json.decode(Client, app.form['data']) or {
		Client{
			name: 'Some client who does not know much about valid JSON.'
		}
	}
	mut new_client := Client{
		key: rand.string(16)
		name: sent_client.name
		mode: 0
		last_active: int(time.now().unix)
	}
	sql app.db {
		insert new_client into Client
	}
	new_client.id = app.db.q_int('SELECT last_insert_rowid()')
	return app.json(json.encode(new_client))
}

// client_list provides the list of all currently connected clients and their state.
['/api/client/list'; post]
pub fn (mut app Vturn) client_list() vweb.Result {
	if check_client(app).mode > 3 {
		clients := sql app.db {
			select from Client
		}
		return app.json(json.encode(clients))
	} else {
		return app.json(json.encode(ApiError{
			message: 'Insufficient permissions'
		}))
	}
}

// client_update takes a client id and a new state and updates the client level
['/api/client/update'; post]
pub fn (mut app Vturn) client_update() vweb.Result {
	if check_client(app).mode > 3 {
		mut sent_client_data := json.decode(Client, app.form['data']) or {
			return app.json(json.encode(ApiError{
				message: 'No data sent'
			}))
		}
		app.db.exec("update client set name = '$sent_client_data.name', mode = $sent_client_data.mode WHERE id == $sent_client_data.id")
		new_client_data := sql app.db {
			select from Client where id == sent_client_data.id
		}
		return app.json(json.encode(new_client_data))
	} else {
		return app.json(json.encode(ApiError{
			message: 'Insufficient permissions'
		}))
	}
	// TODO: websocket push
}
