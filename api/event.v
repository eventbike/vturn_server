module api

import vweb
import json

// event_settings returns an overview of the tournament including the tournament's name and date.
// Settings are encoded as key => value since it should stay rather flexible than fixed in the code
['/api/event/settings'; post]
pub fn (mut app Vturn) event_settings() vweb.Result {
	settings := sql app.db {
		select from Setting
	}
	return app.json(json.encode(settings))
}

// event_data returns all present data about the tournament, including games, statuses, teams and goals. For every team it also returns the id, name, current/next game.
['/api/event/data']
pub fn (mut app Vturn) event_data() vweb.Result {
	return app.json('')
}
