module api

import vweb
import sqlite

const (
	port = 9080
)

struct Vturn {
	vweb.Context
	db sqlite.DB
}

pub fn run(db sqlite.DB) {
	// Struct field 'db' is set to variable 'db' (sqlite.DB) passed to the function
	mut vturn := Vturn{
		db: db
	}
	vweb.run_app<Vturn>(mut vturn, api.port)
}

pub fn (mut app Vturn) init_once() {
}

pub fn (mut app Vturn) init() {
}
